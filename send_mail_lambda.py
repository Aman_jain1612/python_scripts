def lambda_handler(event, context):
	import boto3
	import datetime
	from datetime import timedelta
	from datetime import datetime
	from boto3.session import Session
	import csv
	import itertools
	from email.mime.text import MIMEText
	from email.mime.application import MIMEApplication
	from email.mime.multipart import MIMEMultipart
	session=Session()
	regions=session.get_available_regions('cloudwatch')
	ses_con=boto3.client('ses',region_name='us-east-1')
	with open('/tmp/cpu_utilization_boto.csv','w') as cw:
		csvwriter=csv.writer(cw,delimiter=',')
		data=[ 'Region Name','Instance ID', 'Utilization Status/Average', 'Utilization Status/Maximum','Utilization Status/Minimum']
		csvwriter.writerow(data)
		for reg in regions:
				data=[ reg,'','','','']
				csvwriter.writerow(data)
				try:	
					cloud_con=boto3.client('cloudwatch', region_name=reg)
					ec2_con=boto3.client('ec2', region_name=reg)
					resource=ec2_con.describe_instances()
					starttime=(datetime.utcnow() - timedelta(days=7))
					endtime=datetime.utcnow()
					for reservation in resource["Reservations"]:
					  for instances_id in reservation["Instances"]:
						avg_cpuutil = cloud_con.get_metric_statistics(
						Namespace='AWS/EC2',MetricName='CPUUtilization',
						StartTime=starttime,
						EndTime=endtime, Period=86400,
						Statistics=['Average'], Unit='Percent',
						Dimensions=[{'Name': 'InstanceId', 'Value': instances_id["InstanceId"]}])
						max_cpuutil = cloud_con.get_metric_statistics(
						Namespace='AWS/EC2',MetricName='CPUUtilization',
						StartTime=starttime ,
						EndTime=endtime, Period=86400,
						Statistics=['Maximum'], Unit='Percent',
						Dimensions=[{'Name': 'InstanceId', 'Value': instances_id["InstanceId"]}])
						min_cpuutil = cloud_con.get_metric_statistics(
						Namespace='AWS/EC2',MetricName='CPUUtilization',
						StartTime=starttime ,
						EndTime=endtime, Period=86400,
						Statistics=['Minimum'], Unit='Percent',
						Dimensions=[{'Name': 'InstanceId', 'Value': instances_id["InstanceId"]}])
						avg_cpuutil=avg_cpuutil.values()[0][0].values()[1]
						print avg_cpuutil
						max_cpuutil=max_cpuutil.values()[0][0].values()[1]
						print max_cpuutil
						min_cpuutil=min_cpuutil.values()[0][0].values()[1]
						print min_cpuutil
						data=['' ,instances_id["InstanceId"], avg_cpuutil, max_cpuutil, min_cpuutil ]
						csvwriter.writerow(data)
						
	# and send the message
					
				except: 
					print '%s Region not connecting'%reg
	msg = MIMEMultipart()
	msg['Subject'] = 'Test Mail'
	msg['From'] = 'neeraj.gupta@tothenew.com'
	msg['To'] = 'aman.jain@tothenew.com'
	part = MIMEApplication(open('/tmp/cpu_utilization_boto.csv', "rb").read())
	part.add_header('Content-Disposition', 'attachment', filename='cpu_utilization_boto.csv')
	msg.attach(part)
	ses_con.send_raw_email(RawMessage ={'Data':msg.as_string()}, Source=msg['From'], Destinations=[msg['To']])
def lambda_handler(event, context):
	import boto3
	import datetime
	from datetime import timedelta
	from datetime import datetime
	from boto3.session import Session
	import csv
	import itertools
	from email.mime.text import MIMEText
	from email.mime.application import MIMEApplication
	from email.mime.multipart import MIMEMultipart
	session=Session()
	regions=session.get_available_regions('cloudwatch')
	ses_con=boto3.client('ses',region_name='us-east-1')
	with open('/tmp/cpu_utilization_boto.csv','w') as cw:
		csvwriter=csv.writer(cw,delimiter=',')
		data=[ 'Region Name','Instance ID', 'Utilization Status/Average', 'Utilization Status/Maximum','Utilization Status/Minimum']
		csvwriter.writerow(data)
		for reg in regions:
				data=[ reg,'','','','']
				csvwriter.writerow(data)
				try:	
					cloud_con=boto3.client('cloudwatch', region_name=reg)
					ec2_con=boto3.client('ec2', region_name=reg)
					resource=ec2_con.describe_instances()
					starttime=(datetime.utcnow() - timedelta(days=7))
					endtime=datetime.utcnow()
					for reservation in resource["Reservations"]:
					  for instances_id in reservation["Instances"]:
						avg_cpuutil = cloud_con.get_metric_statistics(
						Namespace='AWS/EC2',MetricName='CPUUtilization',
						StartTime=starttime,
						EndTime=endtime, Period=86400,
						Statistics=['Average'], Unit='Percent',
						Dimensions=[{'Name': 'InstanceId', 'Value': instances_id["InstanceId"]}])
						max_cpuutil = cloud_con.get_metric_statistics(
						Namespace='AWS/EC2',MetricName='CPUUtilization',
						StartTime=starttime ,
						EndTime=endtime, Period=86400,
						Statistics=['Maximum'], Unit='Percent',
						Dimensions=[{'Name': 'InstanceId', 'Value': instances_id["InstanceId"]}])
						min_cpuutil = cloud_con.get_metric_statistics(
						Namespace='AWS/EC2',MetricName='CPUUtilization',
						StartTime=starttime ,
						EndTime=endtime, Period=86400,
						Statistics=['Minimum'], Unit='Percent',
						Dimensions=[{'Name': 'InstanceId', 'Value': instances_id["InstanceId"]}])
						avg_cpuutil=avg_cpuutil.values()[0][0].values()[1]
						print avg_cpuutil
						max_cpuutil=max_cpuutil.values()[0][0].values()[1]
						print max_cpuutil
						min_cpuutil=min_cpuutil.values()[0][0].values()[1]
						print min_cpuutil
						data=['' ,instances_id["InstanceId"], avg_cpuutil, max_cpuutil, min_cpuutil ]
						csvwriter.writerow(data)
						
	# and send the message
					
				except: 
					print '%s Region not connecting'%reg
	msg = MIMEMultipart()
	msg['Subject'] = 'Test Mail'
	msg['From'] = 'neeraj.gupta@tothenew.com'
	msg['To'] = 'aman.jain@tothenew.com'
	part = MIMEApplication(open('/tmp/cpu_utilization_boto.csv', "rb").read())
	part.add_header('Content-Disposition', 'attachment', filename='cpu_utilization_boto.csv')
	msg.attach(part)
	ses_con.send_raw_email(RawMessage ={'Data':msg.as_string()}, Source=msg['From'], Destinations=[msg['To']])
